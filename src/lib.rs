#![feature(asm, lang_items, abi_avr_interrupt)]
#![no_std]

extern crate arduino;

use arduino::{PORTD, TCNT0, TXEN0, UCSR0B, UDR0};
use core::ptr::write_volatile;

const HSYNCPIN: u8 = 3;

const COLORPIN0: u8 = 6;
const COLORPIN1: u8 = 7;

const VSYNCPIN: u8 = 9;

const SKIPLINES: u8 = 35;
const EXTRACOLORPORT: *mut u8 = PORTD;
// const SKIPLINES: u8 = 90;

const VGAX_HEIGHT: usize = 80;
const VGAX_BWIDTH: usize = 24;
const VGAX_WIDTH: usize = VGAX_BWIDTH * 8;
const VGAX_BSIZE: usize = VGAX_BWIDTH * VGAX_HEIGHT;
const VGAX_SIZE: usize = VGAX_WIDTH * VGAX_HEIGHT;
const VGAX_ECSWIDTH: usize = 3;

static mut vtimer: u16 = 0;
static mut aline: u8 = 0;
static mut rlinecnt: u8 = 0;
static mut vskip: u8 = 0;
static vgaxfb: [u8; VGAX_HEIGHT * VGAX_BWIDTH] = [0; VGAX_HEIGHT * VGAX_BWIDTH];
static mut videoline: *mut u8 = 0 as *const u8 as *mut u8;
static vmask: u8 = 0;
// const VGAXFBPTR: *mut u8 = vgaxfb as *mut u8;

// Port pointers
// const PORTD: *mut u8 = 43 as *mut u8;

pub unsafe fn begin() {}

// VSYNC
#[no_mangle]
pub unsafe extern "avr-interrupt" fn _ivr_timer0_compare_a() {
    aline -= 1;
    vskip = SKIPLINES;
    vtimer += 1;
    rlinecnt = 0;
    videoline = vgaxfb.as_ptr() as *const u8 as *mut u8;
}

// HSYNC
#[no_mangle]
pub unsafe extern "avr-interrupt" fn _ivr_timer1_compare_a() {
    if vskip != 0 {
        vskip -= 1;
        return;
    }

    if rlinecnt < VGAX_HEIGHT as u8 {
        const DEJITTER_SYNC: i8 = -0;
        asm!(
"     lds r16, $0
\t     subi r16, $1
\t     andi r16, 7
\t     call TL
\tTL:
\t     pop r31
\t     pop r30
\t     adiw r30, (LW-TL-5)
\t     add r30, r16
\t     ijmp
\tLW:
\t     nop
\t     nop
\t     nop
\t     nop
\t     nop
\tLBEND:
\t"
                :
                : "i"(&TCNT0),
                  "i"(DEJITTER_SYNC)
                : "r30", "r31", "r16", "r17"
                : "volatile");
    }

    fn bit(n: u8) -> u8 {
        1 << n
    }
    unsafe fn nop() {
        asm!("nop");
    }

    static mut p: *mut u8 = videoline;
    static mut c: *mut u8 = p.add(1) as *const u8 as *mut u8;
    fn drawF() {
        unsafe {
            write_volatile(UDR0, c as u8);
            write_volatile(UCSR0B, bit(TXEN0));
            c = p.add(1) as *const u8 as *mut u8;
            nop();
        }
    }
    fn drawS() {
        unsafe {
            write_volatile(UDR0, c as u8);
            write_volatile(EXTRACOLORPORT, vmask);
            for _ in 0..7 {
                nop();
            }
            c = p.add(1) as *const u8 as *mut u8;
        }
    }
    fn draw8() {
        unsafe {
            write_volatile(UDR0, c as u8);
            for _ in 0..12 {
                nop();
            }
            c = p.add(1);
        }
    }
}

pub mod std {
    #[lang = "eh_personality"]
    pub unsafe extern "C" fn rust_eh_personality(
        _state: (),
        _exception_object: *mut (),
        _context: *mut (),
    ) -> () {
    }

    #[panic_handler]
    fn panic(_info: &::core::panic::PanicInfo) -> ! {
        loop {}
    }
}
