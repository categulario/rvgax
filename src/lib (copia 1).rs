#![feature(llvm_asm, asm, lang_items, abi_avr_interrupt)]
#![no_std]

extern crate arduino;

use core::ptr::write_volatile;
use arduino::TCNT0;

const HSYNCPIN: u8 = 3;

const COLORPIN0: u8 = 6;
const COLORPIN1: u8 = 7;

const VSYNCPIN: u8 = 9;

const SKIPLINES: u8 = 90;

const VGAX_HEIGHT: usize = 60;
const VGAX_BWIDTH: usize = 30;
const VGAX_WIDTH: usize = VGAX_BWIDTH * 4;
const VGAX_BSIZE: usize = VGAX_BWIDTH * VGAX_HEIGHT;
const VGAX_SIZE: usize = VGAX_WIDTH * VGAX_HEIGHT;

static mut afreq: u8 = 0;
static mut afreq0: u8 = 0;
static mut vtimer: u16 = 0;
static mut aline: u8 = 0;
static mut rlinecnt: u8 = 0;
static mut vskip: u8 = 0;
static vgaxfb: [u8; VGAX_HEIGHT * VGAX_BWIDTH] = [0; VGAX_HEIGHT * VGAX_BWIDTH];

pub unsafe fn begin() {}

#[no_mangle]
pub unsafe extern "avr-interrupt" fn _ivr_timer0_compare_a() {
    aline -= 1;
    vskip = SKIPLINES;
    vtimer += 1;
    rlinecnt = 0;
}

#[no_mangle]
pub unsafe extern "avr-interrupt" fn _ivr_timer1_compare_a() {
    if vskip != 0 {
        vskip -= 1;
        return;
    }

    if rlinecnt < VGAX_HEIGHT as u8 {
        const DEJITTER_OFFSET: u8 = 1;
        const DEJITTER_SYNC: i8 = -2;

        llvm_asm!(
r"     lds r16, ${timer0}
     subi r16, ${tsync}
     andi r16, 7
     call TL
TL:
     pop r31
     pop r30
     adiw r30, (LW-TL-5)
     add r30, r16
     ijmp
LW:
     nop
     nop
     nop
     nop
     nop
     nop
     nop
LBEND:
"
    :
    : "{timer0}"(&TCNT0),
      "{offset}"(DEJITTER_OFFSET),
      "{tsync}"(DEJITTER_SYNC)
    : "r30", "r31", "r16"
    : "volatile");
    }
}

pub mod std {
    #[lang = "eh_personality"]
    pub unsafe extern "C" fn rust_eh_personality(
        _state: (),
        _exception_object: *mut (),
        _context: *mut (),
    ) -> () {
    }

    #[panic_handler]
    fn panic(_info: &::core::panic::PanicInfo) -> ! {
        loop {}
    }
}
